package com.lj.ovls.controler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/study")
public class StudyController {

	@RequestMapping("/index.html")
	public String toIndex(){
		return "index";
	}
	
	@RequestMapping("/course.html")
	public String toCourse(){
		return "course";
	}
	
}
