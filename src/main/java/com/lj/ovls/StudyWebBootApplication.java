package com.lj.ovls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyWebBootApplication {
	
	public static void main(String[] args){
		SpringApplication.run(StudyWebBootApplication.class, args);
	}
	
}
